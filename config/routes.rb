Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post '/login', to: "sessions#create"
      delete '/logout', to: "sessions#destroy"
      resources :contacts, except: [:new, :edit]
      resources :users, only: :create
    end
  end
  get '*path', to: "application#index"
end
