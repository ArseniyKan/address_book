import React, {Component} from 'react';
import { connect } from 'react-redux';
import Api from '../../services/api';
import { setUser } from '../../redux/actions/user';
import toastr from '../../services/flash';
import './styles.css';

class SignIn extends Component {
  state = {
    loaded: true,
    username: "",
    password: "",
  }

  handleChange = (event) => {
    const {name, value} = event.target;

    this.setState({
      [name]: value,
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const {username, password} = this.state;

    Api('login', {username, password}, 'POST')
      .then(res => {
        const result = res || {};
        const {token, username} = result
        this.props.setUser({token, username});
        this.props.history.push('/');
      })
      .catch(e => {
        toastr.error(e);
      });
  }

  render() {
    return (
      <div className="form-container text-center">
        <form className="form-signin">
          <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
          <label htmlFor="username" className="sr-only">Username</label>
          <input type="text" name="username" className="form-control" onChange={this.handleChange} placeholder="Username"/>
          <label htmlFor="password" className="sr-only">Password</label>
          <input type="password" name="password" className="form-control" onChange={this.handleChange} placeholder="Password"/>
          <button className="btn btn-lg btn-primary btn-block" type="button" onClick={this.handleSubmit}>Sign in</button>
          <button className="btn btn-lg btn-default btn-block" type="button" onClick={() => this.props.history.push("/sign_up")}>Sign up</button>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  ({
    setUser(value) {
      dispatch(setUser(value));
    },
  })

const mapStateToProps = state =>
  ({
    user: state.user,
  });

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
