import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../../../../redux/actions/user';
import './styles.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand navbar-dark fixed-top bg-dark">
        <Link className="navbar-brand" to="/">Address Book</Link>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <span className="navbar-text header-person">{this.props.user.username}</span>
          </li>
          <li className="nav-item">
            <button className="btn btn-warning my-2 my-sm-0" onClick={this.props.logout} type="button">
              <FontAwesomeIcon icon="sign-out-alt" />
            </button>
          </li>
        </ul>
      </nav>
    )
  }
}

const mapDispatchToProps = dispatch =>
  ({
    logout() {
      dispatch(logout());
    },
  })

const mapStateToProps = state =>
  ({
    user: state.user,
  });

export default connect(mapStateToProps, mapDispatchToProps)(Header);
