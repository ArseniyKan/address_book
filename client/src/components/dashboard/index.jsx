import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import ContactList from './pages/contacts/all';
import NewContact from './pages/contacts/new';
import EditContact from './pages/contacts/edit';
import Header from './layout/header';
import './styles.css';

class Dashboard extends Component {
  render() {
    return (
      <div className="container dashboard">
        <Header />
        <div className="row">
          <div className="offset-sm-2 col-8">
            <Route exact path='/' component={ContactList} />
            <Route exact path='/contacts/new' component={NewContact} />
            <Route exact path='/contacts/:id/edit' component={EditContact} />
            <Route exact path='/contacts' component={ContactList} />
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
