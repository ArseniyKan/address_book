import React, {Component} from 'react';
import Api from '../../../../../services/api';
import toastr from '../../../../../services/flash';
import ContactForm from '../forms/contact';

class EditContact extends Component {
  state = {
    contact: {},
    loaded: false,
  }

  componentDidMount() {
    const {id} = this.props.match.params;

    Api(`/contacts/${id}.json`)
      .then(res => {
        const contact = JSON.parse(res.contact);
        this.setState({
          contact,
          loaded: true,
        });
      })
      .catch(e => {
        toastr.error(e);
        this.setState({loaded: true});
      });
  }

  handleChange = (event) => {
    const {name, value} = event.target;
    this.setState({
      contact: {
        ...this.state.contact,
        [name]: value,
      },
    });
  }

  handleChangeArray = (event, type, index) => {
    const items =  Object.assign([], this.state.contact[type]);
    const {name, value} = event.target;
    items[index][name] = value;
    this.setState({
      contact: {
        ...this.state.contact,
        [type]: items,
      },
    });
  }

  handleAdd = (type) => {
    const items = Object.assign([], this.state.contact[type]);
    const item = type === 'phones' ? {kind: 'cell', value: ''} : {value: ''};
    items.push(item);

    this.setState({
      contact: {
        ...this.state.contact,
        [type]: items,
      },
    });
  }

  handleRemove = (index, type) => {
    const items = Object.assign([], this.state.contact[type]);
    const item = items[index];

    if (item.id) {
      items[index]._destroy = true;
    } else {
      items.splice(index, 1);
    }

    this.setState({
      contact: {
        ...this.state.contact,
        [type]: items,
      },
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const { contact } = this.state;

    contact.emails_attributes = contact.emails;
    contact.phones_attributes = contact.phones;

    Api(`contacts/${contact.id}`, { contact }, 'PUT')
      .then(res => {
        const {success, message} = res;

        if (success) {
          toastr.success(message);
          this.props.history.push(`/contacts`);
        }

        this.setState({loaded: true});
      })
      .catch(e => {
        toastr.error(e)
        this.setState({loaded: true});
      });
  }

  handleRemoveContact = () => {
    const {id} = this.props.match.params;

    this.setState({loaded: false});
    Api(`contacts/${id}`, {}, 'DELETE')
      .then(res => {
        const {success, message} = res;

        if (success) {
          toastr.success(message);
          this.props.history.push(`/contacts`);
        }

        this.setState({loaded: true});
      })
      .catch(e => {
        toastr.error(e);
        this.setState({loaded: true});
      });
  }

  render() {
    const {contact, loaded} = this.state;

    if (!loaded) {
      return null;
    }

    return (
      <div className="card">
        <div className="card-body">
          <div className="row">
            <div className="col-6">
              <h5 className="card-title">Edit contact</h5>
            </div>
            <div className="col-6">
              <button className="btn btn-secondary float-right" onClick={this.handleRemoveContact}>Delete contact</button>
            </div>
          </div>
          <hr/>
          <ContactForm
            contact={contact}
            handleChange={this.handleChange}
            handleChangeArray={this.handleChangeArray}
            handleAdd={this.handleAdd}
            handleRemove={this.handleRemove}
            handleSubmit={this.handleSubmit}
          />
        </div>
      </div>
    )
  }
}

export default EditContact;
