import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {PHONE_TYPES} from '../../../../../constants';

class ContactForm extends Component {
  render() {
    const {
      contact: {
        first_name,
        last_name,
        emails,
        phones
      },
      handleRemove,
      handleAdd,
      handleChangeArray,
      handleChange,
      handleSubmit
    } = this.props;
    return (
      <React.Fragment>
        <div className="form-group row">
          <label htmlFor="last_name" className="col-sm-4 col-form-label">Last name</label>
          <div className="col-8">
            <input type="text" value={last_name} onChange={handleChange} name="last_name" className="form-control" placeholder="Last name"/>
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="first_name" className="col-sm-4 col-form-label">First name</label>
          <div className="col-8">
            <input type="text" value={first_name} onChange={handleChange} name="first_name" className="form-control" placeholder="First name"/>
          </div>
        </div>
        <hr/>
        <div className="form-group row">
          <label htmlFor="emails" className="col-sm-4 col-form-label">Emails</label>
          {
            emails.length > 0 && (
              <div className="col-8">
                {
                  emails.map((email, index) => {
                    return (
                      !email._destroy && <div key={index} className="form-row">
                        <div className="form-group col-10">
                          <input type="text" value={email.value} name="value" onChange={(event) => handleChangeArray(event, 'emails', index)} className="form-control" placeholder="Email"/>
                        </div>
                        <div className="form-group col-2">
                          <button className="btn btn-outline-danger float-right" onClick={() => handleRemove(index, 'emails')}>
                            <FontAwesomeIcon icon="trash"/>
                          </button>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            )
          }
          <div className={`${emails.length > 0 ?  'offset-sm-4 col-8' : 'col-8'}`}>
            <button className="btn btn-outline-success float-right" onClick={() => handleAdd('emails')}>
              <FontAwesomeIcon icon="plus"/>
            </button>
          </div>
        </div>
        <hr/>
        <div className="form-group row">
          <label htmlFor="phones" className="col-sm-4 col-form-label">Phones</label>
          {
            phones.length > 0 && (
              <div className="col-8">
                {
                  phones.map((phone, index) => {
                    return (
                      !phone._destroy && <div key={index} className="form-row">
                        <div className="form-group col-4">
                          <select className="form-control" name="kind" onChange={(event) => handleChangeArray(event, 'phones', index)} value={phone.kind}>
                            {
                              PHONE_TYPES.map(type => {
                                const {name, value} = type;
                                return <option key={value} value={value}>{name}</option>
                              })
                            }
                          </select>
                        </div>
                        <div className="form-group col-6">
                          <input type="text" value={phone.value} name="value" onChange={(event) => handleChangeArray(event, 'phones', index)} className="form-control" placeholder="Phone"/>
                        </div>
                        <div className="form-group col-2">
                          <button className="btn btn-outline-danger float-right" onClick={() => handleRemove(index, 'phones')}>
                            <FontAwesomeIcon icon="trash"/>
                          </button>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            )
          }
          <div className={`${phones.length > 0 ?  'offset-sm-4 col-8' : 'col-8'}`}>
            <button className="btn btn-outline-success float-right" onClick={() => handleAdd('phones')}>
              <FontAwesomeIcon icon="plus"/>
            </button>
          </div>
        </div>
        <hr/>
        <button className="btn btn-primary float-right" onClick={handleSubmit}>Save contact</button>
      </React.Fragment>
    )
  }
}

export default ContactForm;
