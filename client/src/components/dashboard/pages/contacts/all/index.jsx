import React, { Component } from 'react';
import Api from '../../../../../services/api';
import toastr from '../../../../../services/flash';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class ContactList extends Component {
  state = {
    contacts: [],
    loaded: false,
    search: '',
  }

  componentDidMount(){
    this.loadingContacts();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.search !== this.state.search) {
      this.loadingContacts();
    }
  }

  loadingContacts = () => {
    const {search} = this.state;

    this.setState({loaded: false});
    Api(`contacts?query=${search}`)
      .then(res => {
        const contacts = res.contacts || [];
        this.setState({
          contacts,
          loaded: true,
        });
      })
      .catch(e =>  {
        toastr.error(e);
        this.setState({loaded: true});
      });
  }

  handleChange = (event) => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  }

  render() {
    const { contacts, loaded } = this.state;

    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">List contacts </h5>
          <hr/>
          <div className="form-group row">
            <div className="col-sm-8">
              <input type="text" className="form-control" name="search" onChange={this.handleChange} placeholder="Search" />
            </div>
            <div className="col-sm-4">
              <Link to={'/contacts/new'}>
              <button type="button" className="btn btn-primary mb-2 float-right">
                <FontAwesomeIcon icon='plus'/>
                {' Add contact'}
              </button>
              </Link>
            </div>
          </div>
          {
            loaded ? (
              <div className="list-group">
                {
                  contacts.map(contact => {
                    const {id, first_name, last_name} = contact;
                    return (
                      <Link to={`/contacts/${id}/edit`} className="list-group-item list-group-item-action" key={contact.id}>
                        {last_name} {first_name}
                      </Link>
                    )
                  })
                }
              </div>
            ) : (
              null
            )
          }
        </div>
      </div>
    )
  }
}

export default ContactList;
