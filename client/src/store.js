import { createStore } from 'redux';
import rootReducer from './redux/reducers';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['user'],
  // stateReconciler: autoMergeLevel2,
};

const inititalState = {};

export const store = createStore(
  persistReducer(persistConfig, rootReducer),
  inititalState
);

export const persistor = persistStore(store);
