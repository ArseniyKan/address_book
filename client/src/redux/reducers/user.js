import { SET_USER, LOGOUT } from '../../constants';

const initialState = {
  token: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGOUT:
      return {
        ...state,
        token: null,
      }
    case SET_USER:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
}
