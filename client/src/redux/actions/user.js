import { SET_USER, LOGOUT } from "../../constants";

export const setUser = (payload) =>
  ({
    type: SET_USER,
    payload,
  })

export const logout = () =>
  ({
    type: LOGOUT,
  })
