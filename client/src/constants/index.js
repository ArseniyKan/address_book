export const SET_USER = 'SET_USER';
export const LOGOUT = 'LOGOUT';

export const PHONE_TYPES = [
  {name: 'Mobile', value: 'cell'},
  {name: 'Work', value: 'work'},
  {name: 'Home', value: 'home'}
];
