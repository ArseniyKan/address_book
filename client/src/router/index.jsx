import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import SignUp from '../components/sign-up';
import SignIn from '../components/sign-in';
import Dashboard from '../components/dashboard';

class Router extends Component {
  render() {
    const { token } = this.props.user;

    return (
      <BrowserRouter>
          {
            token ? (
              <Switch>
                <Route component={Dashboard} />
              </Switch>
            ) : (
              <Switch>
                <Route path="/sign_in" component={SignIn} />
                <Route path="/sign_up" component={SignUp} />
                <Route to="/" component={SignIn} />
              </Switch>
            )
          }
      </BrowserRouter>
    )
  }
}

const mapStateToProps = state =>
  ({
    user: state.user,
  });

export default connect(mapStateToProps)(Router);
