import { store } from '../store';

export default (endPoint, payload = {}, method = 'GET', headers = {}) => {
  let params = {
    method,
  };

  let newHeaders = new Headers(headers);
  newHeaders.append('Content-Type', 'application/json');
  newHeaders.append('Authorization', `Token ${store.getState().user.token}`);
  params.headers = newHeaders;

  if (method !== 'GET') {
    params.body = JSON.stringify(payload);
  }

  return fetch(`${process.env.REACT_APP_API_HOST}${endPoint}`, params)
    .then(response => {

      if (response.status !== 200) { throw "Service unavailable" }

      return response.json();
    })
    .then(json => {
      if (json.success) {
        return json;
      } else {
        throw json.message
      }
    })
    .catch((e) => {
      throw e;
    });
}
