import toastr from 'toastr';
import 'toastr/build/toastr.css';

toastr.options = {
  "positionClass": "toast-top-right",
  "timeOut": "3000",
}

export default toastr;
