if @user.errors.present?
  json.message @user.errors.full_messages.join(". ")
  json.success false
else
  json.token @user.auth_token
  json.username @user.username
  json.success true
end