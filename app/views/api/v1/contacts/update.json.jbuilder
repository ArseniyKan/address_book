if @contact.errors.present?
  json.message @contact.errors.full_messages.join(". ")
  json.success false
else
  json.contact @contact
  json.message "Contact updated"
  json.success true
end