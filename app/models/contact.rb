class Contact < ApplicationRecord
  belongs_to :user

  has_many :contact_items, dependent: :destroy
  has_many :emails, dependent: :destroy
  has_many :phones, dependent: :destroy

  accepts_nested_attributes_for :emails, :phones, allow_destroy: true, reject_if: proc { |attributes| attributes['value'].blank? }

  validates_presence_of :first_name, :last_name

  scope :find_by_query, -> (query) { joins(:contact_items).where("contacts.first_name ILIKE :query OR contacts.last_name ILIKE :query OR contact_items.value ILIKE :query", query: "%#{query}%").distinct }
end
