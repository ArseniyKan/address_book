class Phone < ContactItem
  enum kind: { cell: 0, work: 1, home: 2 }

  validates_presence_of :kind
end
