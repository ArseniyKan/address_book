class ContactItem < ApplicationRecord
  belongs_to :contact, required: false

  validates_presence_of :type, :value
end
