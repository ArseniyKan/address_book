class User < ApplicationRecord
  has_many :contacts, dependent: :destroy
  has_secure_password
  has_secure_token :auth_token

  validates_presence_of :username
  validates_uniqueness_of :username

  def invalidate_token
    self.update_column(:auth_token, nil)
  end

  def self.validate_login(params)
    user = User.find_by_username(params[:username])

    if user && user.authenticate(params[:password])
      user
    end
  end
end
