module Api
  module V1
    class UsersController < BaseController
      before_action :authenticate_user, except: :create

      def create
        @user = User.create(user_params)
      end

      private

      def user_params
        params.require(:user).permit(:username, :password)
      end
    end
  end
end
