module Api
  module V1
    class BaseController < ActionController::API
      include ActionController::HttpAuthentication::Token::ControllerMethods

      before_action :authenticate_user

      def authenticate_user
        authenticate_token || render_unauthorized
      end

      def current_user
        @current_user ||= authenticate_token
      end

      protected

      def render_unauthorized
        render json: { message: "Wrong username or password", success: false }
      end

      private

      def authenticate_token
        authenticate_with_http_token do |token, options|
          User.find_by(auth_token: token)
        end
      end
    end
  end
end
