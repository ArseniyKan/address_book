module Api
  module V1
    class ContactsController < BaseController
      before_action :set_contact, only: [:show, :update, :destroy]

      def index
        @contacts = current_user.contacts
        @contacts = @contacts.find_by_query(params[:query]) if params[:query].present?
      end

      def show
      end

      def create
        @contact = Contact.new(contact_params)
        @contact.user = current_user
        @contact.save
      end

      def update
        @contact.update_attributes(contact_params)
      end

      def destroy
        @contact.destroy
      end

      private

      def set_contact
        @contact = Contact.find(params[:id])
      end

      def contact_params
        params.require(:contact).permit(
          :user_id,
          :first_name,
          :last_name,
          emails_attributes: [:id, :value, :_destroy],
          phones_attributes: [:id, :value, :_destroy, :kind]
        )
      end
    end
  end
end
