module Api
  module V1
    class SessionsController < BaseController
      skip_before_action :authenticate_user, only: :create, raise: false

      def create
        if user = User.validate_login(session_params)
          user.regenerate_auth_token
          render json: { token: user.auth_token, username: user.username, success: true }
        else
          render_unauthorized
        end
      end

      def destroy
        current_user.invalidate_token
        render json: {}
      end

      private

      def session_params
        params.require(:session).permit(:username, :password)
      end
    end
  end
end
