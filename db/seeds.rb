User.destroy_all

5.times do |i|
  user = User.create(username: "test#{i + 1}", password: 'test')
  5.times do
    contact = Contact.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, user_id: user.id)
    2.times do
      Email.create(value: Faker::Internet.email, contact_id: contact.id)
    end
    3.times do
      Phone.create(value: Faker::PhoneNumber.phone_number, kind: (rand() * 3).to_i, contact_id: contact.id)
    end
  end
end
