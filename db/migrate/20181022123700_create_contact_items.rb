class CreateContactItems < ActiveRecord::Migration[5.0]
  def change
    create_table :contact_items do |t|
      t.references :contact, foreign_key: true
      t.integer :kind
      t.string :value
      t.string :type

      t.timestamps
    end
  end
end
